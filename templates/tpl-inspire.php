<?php
/*
//Template Name: tpl moulinette
*/

?>


<?php get_header();

    $arg = array(
        'posts_per_page'    => -1,
        'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash') , 
        'post_type'         => 'training',
    );

    //If get_post_type() = training
        $arg['tax_query']  = array(
            array(
                'taxonomy' => 'taxo_reference',
                'field'    => 'slug',
                'terms'    => 'expression-artistique',
            )
        );			

    $posts = get_posts( $arg );
    ?>

    <?php 
    foreach($posts as $post): 
    
        setup_postdata( $post );
        $post->post_type = 'inspire';
        

        //taxo mot-clés
        $terms_taxo_tag= get_the_terms($post, 'taxo_tag') ;
        $t=array();
        if ($terms_taxo_tag):
            foreach ($terms_taxo_tag as $term):
                $term_tag_inspire = get_term_by( 'slug', $term->slug, 'taxo_tag_inspire' );
                if ($term_tag_inspire === false ){
                    $term_tag_inspire = wp_insert_term($term->name, 'taxo_tag_inspire', array('description'=>$term->description, 'slug'=>$term->slug));
                    $t[] = $term_tag_inspire['term_id'];
                }
                else{
                    $t[] = $term_tag_inspire->term_id;
                }
            endforeach;
        endif;
        wp_set_post_terms( $post->ID, $t, 'taxo_tag_inspire' );
        wp_set_post_terms( $post->ID, array(), 'taxo_tag' );

        //taxo catégorie
        $terms_taxo_training= get_the_terms($post, 'taxo_training') ;
        $t=array();
        if ($terms_taxo_training):
            foreach ($terms_taxo_training as $term):
                $term_taxo_inspire = get_term_by( 'slug', $term->slug, 'taxo_inspire' );
                if ($term_taxo_inspire === false ){
                    $term_taxo_inspire = wp_insert_term($term->name, 'taxo_inspire', array('description'=>$term->description, 'slug'=>$term->slug));
                    $t[] = $term_taxo_inspire['term_id'];
                }
                else{
                    $t[] = $term_taxo_inspire->term_id;
                }
            endforeach;
        endif;
        wp_set_post_terms( $post->ID, $t, 'taxo_inspire' );
        wp_set_post_terms( $post->ID, array(), 'taxo_training' );

        //taxo référence
        $terms_taxo_reference= get_the_terms($post, 'taxo_reference') ;
        $t=array();
        if ($terms_taxo_reference):
            foreach ($terms_taxo_reference as $term):
                $term_taxo_reference_inspire = get_term_by( 'slug', $term->slug, 'taxo_reference_inspire' );
                if ($term_taxo_reference_inspire === false ){
                    $term_taxo_reference_inspire = wp_insert_term($term->name, 'taxo_reference_inspire', array('description'=>$term->description, 'slug'=>$term->slug));
                    $t[] = $term_taxo_reference_inspire['term_id'];
                }
                else{
                    $t[] = $term_taxo_reference_inspire->term_id;
                }
            endforeach;
        endif;
        wp_set_post_terms( $post->ID, $t, 'taxo_reference_inspire' );
        wp_set_post_terms( $post->ID, array(), 'taxo_reference' );

        wp_update_post($post);
    endforeach;
    wp_reset_postdata();
   
get_footer();
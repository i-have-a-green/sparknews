<?php
/*
Template Name: tpl archive
*/
?>

<?php get_header(); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="has-random-color">

	<div id="two-columns-layout" class="custom-color-bg">

		<!-- Header -->
		<div id="column-left">
			
			<header class="wrapper">
				<?php 
				wpBreadcrumb();
				echo '<h1>'; 
				lettrine($post); 
				echo get_the_title() .'</h1>'; 
				?>
			</header>

			<div id="raw-content">
				<?php the_content(); ?>
			</div>
		</div>

		<!--  Filters -->
		<div id="column-right" class="wrapper">

		<?php 
			$cpt = get_field('custom_post_type');
			set_query_var( 'cpt', $cpt );
			get_template_part( 'template-parts/part','taxo' );
		
			$arg = array(
				'posts_per_page'    => get_option('posts_per_page' ),
				'post_status'       => 'publish',
				'post_type'         => $cpt,
			);

			//If get_post_type() = training
			if(isset($_GET['var_taxo_tag']) && !empty($_GET['var_taxo_tag']) ) {
				if($cpt == 'inspire'){
					$arg['tax_query']  = array(
						array(
							'taxonomy' => 'taxo_tag_inspire',
							'field'    => 'term_id',
							'terms'    => $_GET['var_taxo_tag'],
						)
					);
				}
				else{
					$arg['tax_query']  = array(
						array(
							'taxonomy' => 'taxo_tag',
							'field'    => 'term_id',
							'terms'    => $_GET['var_taxo_tag'],
						)
					);
				}
				
				// var_dump($arg);
			}

			$posts = get_posts( $arg );
			if($posts):

			// <!-- pour le scroll -->
			$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
		?>
		</div><!--  End of Filters -->

	</div><!--  End of #two-columns-layout -->

	<!-- Listing Archive -->
	<section >

		<!-- wrapper -->
		<div class="listing-archive wrapper v-padding-small"
			
		data-cpt=<?php echo $cpt; ?>
		data-page="<?php echo $num_page;?>"
		data-url="<?php echo get_the_permalink();?>"
		data-taxo=""
		data-taxo_tag="<?php if(isset($_GET['var_taxo_tag'])): echo $_GET['var_taxo_tag']; endif;?>"
		id="infinite-list">
		
			<?php foreach($posts as $post): 
			
				// var_dump($post);
				setup_postdata( $post );

				get_template_part( 'template-parts/archive', get_post_type() );

			endforeach; 

			wp_reset_postdata();
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;

		?>

		</div><!-- /wrapper -->

	</section><!-- End of Listing Archive -->

</div><!-- /#has-random-color -->

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();

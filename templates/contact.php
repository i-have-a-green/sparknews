<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<div id="has-random-color">

  <header class="wrapper custom-color-bg">
    <?php 
    wpBreadcrumb();
    echo '<h1 class="padding-b" >'; 
    lettrine($post); 
    echo get_the_title() .'</h1>'; 
    ?>
  </header>
          
  <!-- Begining of the loop -->
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <main id="raw-content">
    <?php the_content(); ?>
  </main>

</div><!-- /#has-random-color -->

<!-- Contact Form -->
<!-- 
<div class="v-padding-regular wrapper center">
  <form class="left" name="contactForm" id="contactForm" action="#" method="POST">

    <input type="hidden" name="nonceformContact" value="">
    
    <label for="nameContact"><?php esc_html_e('Nom', 'sparknews')?> *</label>
    <input type="text" name="nameContact" id="nameContact" placeholder="<?php esc_html_e('Martin', 'sparknews')?>" required value="">

    <label for="firstNameContact"><?php esc_html_e('Prénom', 'sparknews')?> *</label>
    <input type="text" name="firstNameContact" id="firstNameContact" placeholder="<?php esc_html_e('Jean', 'sparknews')?>" required value="">

    <label for="nameEntreprise"><?php esc_html_e('Entreprise', 'sparknews')?></label>
    <input type="text" name="nameEntreprise" id="nameEntreprise" placeholder="<?php esc_html_e('Nom de la société', 'sparknews')?>" value="">

    <label for="emailContact"><?php esc_html_e('Adresse mail', 'sparknews')?> *</label>
    <input type="email" name="emailContact" id="emailContact" placeholder="<?php esc_html_e('nom.prenom@exemple.com', 'sparknews')?>" required value="">

    <label for="tel"><?php esc_html_e('Téléphone', 'sparknews')?></label>
    <input type="tel" name="tel" id="tel" placeholder="06 12 34 56 78" value="">

    <p class="label-like margin-t"><?php esc_html_e('Vous êtes', 'sparknews')?></p>

    <div class="custom-radio-button">
      <input type="radio" id="identityChoice1" name="identity" value="Créateur indépendant" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice1"><?php esc_html_e('Créateur / Créatrice indépendant(e)', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="identityChoice2" name="identity" value="Créateur salarié" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice2"><?php esc_html_e('Créateur / Créatrice salarié(e)', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="identityChoice3" name="identity" value="Structure d'accompagnement" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice3"><?php esc_html_e('Structure d’accompagnement à la création', 'sparknews')?></label>
    </div>

    <p class="label-like margin-t"><?php esc_html_e('Vous souhaitez', 'sparknews')?>  *</p>

    <div class="custom-radio-button">
      <input type="radio" id="whishChoice1" name="wish" value="Rejoindre la Fabrique des Récits (et recevoir la newsletter)" class="input-radio-button" required>
      <label class="custom-radio-label" for="wishChoice1"><?php esc_html_e('Rejoindre le collectif de la Fabrique des Récits (et recevoir notre newsletter pour être tenu(e) au courant de notre actualité)', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="whishChoice3" name="wish" value="Demande d’informations" class="input-radio-button" >
      <label class="custom-radio-label" for="wishChoice2"><?php esc_html_e('Faire une demande d’informations', 'sparknews')?></label>
    </div>

    <p class="label-like margin-t"><?php esc_html_e('Êtes-vous déjà engagé(e) sur les sujets de transition écologique et sociale ?', 'sparknews')?></p>
      
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice1" name="ecology" value="Je suis sceptique" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice1"><?php esc_html_e('Je suis sceptique', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice2" name="ecology" value="Je découvre" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice2"><?php esc_html_e('Je découvre', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice3" name="ecology" value="Je suis en réflexion" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice3"><?php esc_html_e('Je suis en réflexion', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice4" name="ecology" value="Je ne sais pas comment faire" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice4"><?php esc_html_e('Je ne sais pas comment faire', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice5" name="ecology" value="J\'ai commencé" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice5"><?php esc_html_e('J\'ai commencé', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="radio" id="ecologyChoice6" name="ecology" value="Je suis très investi(e)" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice6"><?php esc_html_e('Je suis très investi(e)', 'sparknews')?></label>
    </div> 
    
    <p class="label-like margin-t"><?php esc_html_e('Quels sont les sujets qui vous intéressent le plus ?', 'sparknews')?></p>
      
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice1" name="topics[]" value="Production & consommation" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice1"><?php esc_html_e('Production & consommation', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice2" name="topics[]" value="Agriculture & Alimentation" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice2"><?php esc_html_e('Agriculture & Alimentation', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice3" name="topics[]" value="Société" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice3"><?php esc_html_e('Société', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice4" name="topics[]" value="Santé" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice4"><?php esc_html_e('Santé', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice5" name="topics[]" value="Démocratie" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice5"><?php esc_html_e('Démocratie', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice6" name="topics[]" value="Énergie" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice6"><?php esc_html_e('Énergie', 'sparknews')?></label>
    </div> 
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice7" name="topics[]" value="Éducation & Travail" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice7"><?php esc_html_e('Éducation & Travail', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice8" name="topics[]" value="Aménagement de l\'espace" class="input-radio-button" >
      <label class="custom-radio-label" for="identityChoice8"><?php esc_html_e('Aménagement de l\'espace', 'sparknews')?></label>
    </div>
    <div class="custom-radio-button">
      <input type="checkbox" id="topicsChoice9" name="topics[]" value="Rapport à la Nature" class="input-radio-button">
      <label class="custom-radio-label" for="identityChoice9"><?php esc_html_e('Rapport à la Nature', 'sparknews')?></label>
    </div>

    <label class="margin-t" for="messageAttente"><?php esc_html_e('Vos attentes', 'sparknews')?></label>  
    <textarea name="messageAttente" id="messageAttente" placeholder="<?php esc_html_e('Qu’attendez-vous de la Fabrique des Récits ?', 'sparknews')?>" rows="8"></textarea>

    <label class="margin-t" for="messageContact"><?php esc_html_e('Message', 'sparknews')?></label>  
    <textarea name="messageContact" id="messageContact" placeholder="<?php esc_html_e('Rédigez votre message', 'sparknews')?>" rows="8"></textarea>
    
    <p class="body-like font-title margin-t">* <?php esc_html_e('Mentions obligatoires', 'sparknews')?></p>
  
    <input type="checkbox" required name="rgpdCheckbox" id="rgpdCheckbox" class="custom-checkbox">
    <label class="checkbox-label body-like" for="rgpdCheckbox">
      <span class="checkmark"></span>
      <p><?php _e( 'J’accepte le traitement de mes données personnelles. En savoir plus sur', 'leksi' ); ?> <a href="<?php echo get_privacy_policy_url();?>"><?php _e( 'notre politque de confidentialité', 'leksi' ); ?></a>.</p>
    </label>
  
    <div id="ResponseAnchor" class="margin-t">
      <input class="button brand-blue" type="submit" id="sendMessage" value="Envoyer le message">
    </div>

  </form>

</div> 
-->


<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>

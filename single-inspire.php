<?php
/**
 * Template part for displaying page single-training in page.php
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php get_header(); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="has-random-color">

	<div id="scroll-content">

		<!-- Header -->
		<header id="scroll-header" class="wrapper">
			<?php wpBreadcrumb();?>
			<?php
			// Type of article (training)
			if(get_post_type() == "inspire"):
				$ref = ihag_get_term($post, 'taxo_reference');
				//echo '<p class="article-type h3-like">'. $ref->name .'</p>';
			endif;
			?>
			
			<?php
			// Title with icon (Stratus)
			$icon = get_field('activate_icon');

			if ( $icon ) {

				$style = get_field('icon_style');
				if (!$style) {
					$style = 'style_1';
					// If no style selected, use style_1 by default
				}

				echo '<h1 class="title_with_icon '. $style .'">';
				the_title();
				echo '</h1>';

			} else {
				the_title('<h1>', '</h1>');
			}
			?>

		</header>

		<!-- Info 1 -->
		<?php get_template_part('template-parts/informations-article'); ?>

		<main id="post-content" class="wrapper title-custom-color">
			<?php the_content(); ?>
		</main>

	</div>

	<!-- Suggestions -->
	<section class="v-padding-regular">

		<h2 class="h1-like wrapper"><?php esc_html_e('Dans la même catégorie', 'sparknews')?></h2>

		<!-- Lisitng -->
		<div class="listing-archive">

		<?php 

			$term = ihag_get_term($post, 'taxo_training');
			// var_dump($terms );

			$cat = array(
				'posts_per_page' => 2,
				'post_status' 	 => 'publish',
				'post_type'		 => 'inspire',

				'exclude' => array($post->ID),
				'tax_query' => array(
					array(
						'taxonomy' => 'taxo_inspire',
						'field'    => 'slug',
						'terms'    => array($term->slug)
					)
				)
			);
			// var_dump($cat);
			$posts = get_posts( $cat );

			// var_dump($posts );

			foreach($posts as $post): 
				
				setup_postdata( $post );
				// var_dump($post );

				get_template_part( 'template-parts/standard', get_post_type() );

			endforeach; 

			wp_reset_postdata();?>

		</div><!-- /listing -->
	</section>
	
</div><!-- /#has-random-color -->

<?php endwhile; ?>
<?php endif;?>

<?php get_footer(); ?>

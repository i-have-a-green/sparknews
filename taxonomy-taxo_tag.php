<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<!-- Header -->
<header class="wrapper">
	<?php //wpBreadcrumb(); ?>
	<h1>#<?php echo single_cat_title( '', false );?></h1>
	<div class="archive-info h1-like">
		<?php echo category_description(); ?>
	</div>
</header>

<!-- pour le scroll -->
<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>
<?php //var_dump($wp_query->found_posts); ?>
				

<section class="wrapper listing-universal" >

<?php if ( have_posts() ) : ?>
<!-- Listing Universal -->
<p class="h1-like left"><?php esc_html_e('Tous les articles sur le sujet : ', 'sparknews')?></p>
	<div class="v-padding-small" 
		data-cpt=""
		data-page="<?php echo $num_page;?>"
		data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
		data-url="<?php echo get_category_link(get_queried_object()->term_id);?>"
		data-taxo=""
		data-taxo_tag="<?php echo get_queried_object()->term_id;?>"

		id="infinite-list">

		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/standard', get_post_type() );
		endwhile;
		?>

	</div>

<?php else : ?>

<p class="h1-like left"><?php esc_html_e('Aucun article correspondant', 'sparknews')?></p>
<?php get_template_part( 'template-parts/content', 'none' ); ?>

<?php endif; ?>

</section><!-- End of Listing Archive -->

<?php
get_footer();

<?php get_header(); ?>

<?php $block_uniq_id = "id_".uniqid(); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php

	//echo '<div id="has-random-color" class="img-white-and-black">';
	echo '<div id="has-random-color">';
		
?>
<main id="raw-content">
	<section id="blk-header-home" class="center brand-violet-bg">
		<div class="logo_text">
			<div id="logo-home">
				<img id="stay" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_base.svg" alt="La Fabrique des récits">
				<img id="slide0" class="active" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_00.svg" alt="#">
				<img id="slide1" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_01.svg" alt="#">
				<img id="slide2" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_02.svg" alt="#">
				<img id="slide3" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_03.svg" alt="#">
				<img id="slide4" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_04.svg" alt="#">
				<img id="slide5" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_05.svg" alt="#">
				<img id="slide6" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_06.svg" alt="#">
				<img id="slide7" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_07.svg" alt="#">
				<img id="slide8" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_08.svg" alt="#">
				<img id="slide9" src="<?php echo get_template_directory_uri(); ?>/image/logo_homepage_09.svg" alt="#">
			</div>	
			<div class="text_zone">
				<?php
					$text = get_field('text');
					if($text) {
						// Title with icon (Stratus)
						$icon = get_field('activate_icon');
						if ( $icon ) {
							$style = get_field('icon_style');
							if (!$style) {
								$style = 'style_1';
								// If no style selected, use style_1 by default
							}
							echo '<h2 class="h1-like margin-b title_with_icon '. $style .'">'. $text .'</h2>';
						} else {
							echo '<h2 class="h1-like margin-b">'. $text .'</h2>';
						}
					}?>
			</div>
		</div>
		
		<div class="video_zone">
			<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-video ">

				<div class="">

					<?php
					$video = get_field('link_video');
					if ( !$video ) :?>
						<em>Renseigner le bloc</em>
						
					<?php else :?>

						<div class="video-container">

							<?php 
							// 01 -Test if thumbnail_video doesn't exists
							if (!get_field('thumbnail_video')) {    
							?>
								<a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
									<script> 
										if (typeof iframe === 'undefined') {
										var iframe = new Object();
										}
										iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
									</script>

									<?php
										$video = get_field( 'link_video' );
										$id = retrieve_id_video($video);
									?>

									<img src="https://img.youtube.com/vi/<?php echo $id; ?>/mqdefault.jpg">

								</a> 
							
							<?php 
							} else { 
							// 02 - If we have a thumbnail
							?>

								<a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
									<script> 
										if (typeof iframe === 'undefined') {
											var iframe = new Object();
										}
										iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
									</script>

									<?php
									$videoThumbnail = get_field('thumbnail_video');
									$size = 'free-height';
									echo wp_get_attachment_image($videoThumbnail, $size);?>
								</a>

							<?php 
							} ?>
						</div><!-- /embed-container -->
					<?php endif; ?>
				</div><!-- /wrapper-->
			</section>
		</div>
	</section>
	<?php the_content(); ?>
</main>

<?php if ( !is_front_page() ){

echo '</div><!--/#has-random-color -->';	
} ?>


<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();



<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

<div id="has-random-color">

	<!-- recup name cpt -->
	<?php $cpt = substr(get_queried_object()->taxonomy, 5); ?>

	<div id="two-columns-layout" class="custom-color-bg">
		<!-- Header -->
		<div id="column-left" class="wrapper">
			<header class="cat-title no-breadcrumb">
				<?php //wpBreadcrumb(); ?>
				<p class="h1-like"><?php echo get_the_title(get_field('archive_'.$cpt , 'option'));?><p>
				<h1>
					<?php 
					lettrine( get_queried_object() );
					echo single_cat_title('', false );
					?>
				</h1>

				<div class="archive-info h1-like">
					<?php echo category_description(); ?>
				</div>
			</header>
		</div>

		<!--  Filters -->
		<div id="column-right" class="wrapper">

			<?php if ( have_posts() ) :
				set_query_var( 'cpt', $cpt );
				get_template_part( 'template-parts/part','taxo' ); 
			endif; ?>

			<!-- pour le scroll -->
			<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>

		</div><!--  End of Filters -->
	</div><!--  End of #two-columns-layout -->

</div><!-- /#has-random-color -->

<!-- Listing Archive -->
<section class="listing-archive v-padding-small"

	data-cpt=<?php echo $cpt ; ?>
	data-page="<?php echo $num_page;?>"
	data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
	data-url="<?php echo get_category_link(get_queried_object()->term_id);?>" 
	data-taxo="<?php echo get_queried_object()->term_id;?>"
	data-taxo_tag="<?php if(isset($_GET['var_taxo_tag'])): echo $_GET['var_taxo_tag']; endif;?>"

	id="infinite-list">

	<?php if ( have_posts() ) : ?>

		<?php //var_dump($wp_query->found_posts);?>
		<?php
		/* Start the Loop */
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/archive', $cpt  );
		endwhile;
		?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/content', 'none' ); ?>

	<?php endif; ?>


</section><!-- End of Listing Archive -->

<?php
get_footer();

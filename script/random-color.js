document.addEventListener('DOMContentLoaded', function() {

    var randomColor = document.getElementById("has-random-color");

    if (randomColor) {

        // Choisis un chiffre au hasard entre 1 et 6 
        var rand = Math.floor(Math.random() * 6) + 1 ;
        var color = 'random-blue';

        if (rand == 1) {
            color = 'random-blue';
        } else if (rand == 2) {
            color = 'random-orange';
        } else if (rand == 3) {
            color = 'random-pink';
        } else if (rand == 4) {
            color = 'random-green';
        } else if (rand == 5) {
            color = 'random-yellow';
        } else if (rand == 6) {
            color = 'random-violet';
        } else {
            console.log('Sorry, random number selection has failled');
        }

        /*
        // Debug
        console.log('Nombre alétoire :');
        console.log(rand);
        console.log('Couleur :');
        console.log(color);
        */

        randomColor.classList.add(color);
    }    
});
document.addEventListener('DOMContentLoaded', function() {
    var title = document.getElementsByClassName("title_with_icon");
    var letters = 5;

    Array.prototype.forEach.call(title, function(string) {
        var str = string.innerText; 


        var cleanContent = str.replace( /[\r\n]+/gm, " " ); 

        var newContent = cleanContent.split(" ").map( function (x) {
        if (x.length > letters) {
            return "<span>" + x + "</span>";
        } else {
            return x;
        }
        });
    
        string.innerHTML = newContent.join(" ");
    });

});
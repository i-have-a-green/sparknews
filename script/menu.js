/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

/*

Sticky content for single.php, single-training.php & single-tools.php

*/

window.addEventListener('scroll', function(e) {

    var container = document.getElementById("scroll-content");

    if (container) {

        var menu = document.getElementById("sticky-content");
        var stickyStart = container.offsetTop;
        var stickyStop = container.offsetHeight;
        var menuHeight = menu.offsetHeight;
        var derniere_position_de_scroll_connue = 0;
        var ticking = false;

        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
    
                if (window.pageYOffset > stickyStart) {
                    // Scroll start
                    menu.classList.add("sticky");
    
                    if (window.pageYOffset > ( stickyStart + stickyStop - menuHeight )) {
                        // Scroll after container
                        menu.classList.add("sticky-finish");
                    } else {
                        // Scroll after container
                        menu.classList.remove("sticky-finish");
                    }
                } else {
                    // Scroll didn't start
                    menu.classList.remove("sticky");
                }
            }, 50); //fréquence du scroll
        }

        ticking = true;
    }
    
});


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/

// var derniere_position_de_scroll_connue = 0;
// var ticking = false;
// var menu = document.getElementById("scroll-anchor");
// var header = document.getElementById("topbar-wrapper");
// var sticky = menu.offsetTop;

// window.addEventListener('scroll', function(e) {
//     if (!ticking) {
//         window.setTimeout(function() {
//             ticking = false;
//             if (window.scrollY > sticky) {
//                 header.classList.add("scroll");
//             } else {
//                 header.classList.remove("scroll");
//             }
//         }, 300); //fréquence du scroll
//     }
//     ticking = true;
// });
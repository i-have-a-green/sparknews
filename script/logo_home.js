
var prefix = "slide";
var slidecount = 0;
var bgcount = 0;
var blkHeaderHome = document.getElementById("blk-header-home");

if ( blkHeaderHome ) {

    // Animation on click
    blkHeaderHome.addEventListener('click', function(e) {

        e.preventDefault();
        animateLogo();

    });

    // Animation on hover
    var logo = document.getElementById("logo-home");
    logo.addEventListener('mouseenter', function(e){

        animateLogo();

    });
}

function animateLogo() {

        // On anime la couleur d'arrière-plan
        if (bgcount <= 5) {
            
            if (bgcount < 5) {
                if (bgcount == 0) {
                    // Step 2 : blue background 
                    blkHeaderHome.classList.remove("brand-violet-bg");
                    blkHeaderHome.classList.add("brand-blue-bg");
                }
                if (bgcount == 1) {
                    // Step 3 : green background 
                    blkHeaderHome.classList.remove("brand-blue-bg");
                    blkHeaderHome.classList.add("brand-green-bg");
                }
                if (bgcount == 2) {
                    // Step 4 : yellow background 
                    blkHeaderHome.classList.remove("brand-green-bg");
                    blkHeaderHome.classList.add("brand-violet-bg");
                }
                if (bgcount == 3) {
                    // Step 5 : orange background 
                    blkHeaderHome.classList.remove("brand-violet-bg");
                    blkHeaderHome.classList.add("brand-orange-bg");
                }
                if (bgcount == 4) {
                    // Step 6 : pink background 
                    blkHeaderHome.classList.remove("brand-orange-bg");
                    blkHeaderHome.classList.add("brand-pink-bg");
                }
                bgcount++;

            } else {
                // Step 1 & 7 : violet background
                bgcount = 0;
                blkHeaderHome.classList.remove("brand-pink-bg");
                blkHeaderHome.classList.add("brand-violet-bg");
            }
        }


        // On anime le logo
        if (slidecount <= 9) {
            
            var currentslide = prefix.concat(slidecount);
            var nextslide = prefix.concat(slidecount+1);
            var firstslide = prefix.concat('0');
            
            if (slidecount < 9) {
                // Pour les Slides de 1 à 8 : on fait apparaitre la slide suivante
                document.getElementById(currentslide).classList.remove("active");
                document.getElementById(nextslide).classList.add("active");
                slidecount++;
            } else {
                // Pour la slide 8 : on fait réapparaitre la slide 1
                document.getElementById(currentslide).classList.remove("active");
                document.getElementById(firstslide).classList.add("active");
                slidecount = 0;
            }

        } else {
            slidecount = 0;
            console.log ("Erreur dans l'animation du logo");
        }

        logo.classList.add("active");
        window.setTimeout( function() {
            logo.classList.remove("active");
        }, 500); //fréquence du scroll
}
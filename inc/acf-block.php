<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'sparknews',
                'title' => __( 'SparkNews', 'sparknews' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(
            array(
                'name'				    => 'titre',
                'title'				    => __('Titre'),
                'description'		    => __('Titre h1, h2, h3 ou h4'),
                'placeholder'		    => __('Titre'),
                'render_template'	    => 'template-parts/block/titre.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'menu',
                'keywords'			    => array('titre', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'paragraphe',
                'title'				    => __('Paragraphe'),
                'description'		    => __('Bloc de texte (éditeur de contenus)'),
                'placeholder'		    => __('Paragraphe'),
                'render_template'	    => 'template-parts/block/paragraphe.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'text',
                'keywords'			    => array('paragraphe', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'texte',
                'title'				    => __('Texte'),
                'description'		    => __('Bloc Gutenberg qui permet d’afficher un titre h2 + contenu texte (avec l’éditeur de contenus : liens et texte en gras autorisé) + hyperlien [en option] + Libellé du lien [en option]'),
                'placeholder'		    => __('Texte'),
                'render_template'	    => 'template-parts/block/texte.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('titre', 'texte', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        
        acf_register_block_type(
            array(
                'name'				    => 'lead-paragraphe',
                'title'				    => __('Lead paragraphe'),
                'description'		    => __('Paragraphe de description de la page'),
                'placeholder'		    => __('Lead-paragraphe'),
                'render_template'	    => 'template-parts/block/lead-paragraphe.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('lead', 'paragraphe', 'text'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'titre-texte-image',
                'title'				    => __('Titre + texte + image'),
                'description'		    => __('Bloc Gutenberg qui permet d’afficher un titre h2 + contenu texte (avec l’éditeur de contenus : liens et texte en gras autorisé) + image +  hyperlien [en option] + Libellé du lien [en option]. Sur desktop, le bloc peut être affiché soit 
                “texte - image” soit “image - texte” en activant une option dans l’admin.'),
                'placeholder'		    => __('Titre + texte + image'),
                'render_template'	    => 'template-parts/block/titreTexteImage.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('titre', 'texte', 'image', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'image',
                'title'				    => __('Image'),
                'description'		    => __('Affiche une image'),
                'placeholder'		    => __('image'),
                'render_template'	    => 'template-parts/block/image.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'format-image',
                'keywords'			    => array('agence', 'bloc', 'image'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'video',
                'title'				    => __('Vidéo'),
                'description'		    => __('Vidéo à partir d\'un lien (ex :Youtube, Dailymotion, Viméo …)'),
                'placeholder'		    => __('video'),
                'render_template'	    => 'template-parts/block/video.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'format-video',
                'keywords'			    => array('agence', 'bloc', 'video'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

       
        acf_register_block_type(
            array(
                'name'				    => 'evenement',
                'title'				    => __('Évènement'),
                'description'		    => __('Prochains évènements de la Fabrique'),
                'placeholder'		    => __('Evènement'),
                'render_template'	    => 'template-parts/block/evenement.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'calendar-alt',
                'keywords'			    => array('titre', 'évènement', 'information', 'texte', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );
        
        
        acf_register_block_type(
            array(
                'name'				    => 'evenement-video',
                'title'				    => __('Évènement-vidéo'),
                'description'		    => __('Prochains évènements de la Fabrique'),
                'placeholder'		    => __('Evènement'),
                'render_template'	    => 'template-parts/block/evenement-video.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'calendar-alt',
                'keywords'			    => array('titre', 'évènement', 'information', 'texte', 'lien', 'video'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'source-article',
                'title'				    => __('Source article'),
                'description'		    => __('Lien vers la source de l\'article'),
                'placeholder'		    => __('Source'),
                'render_template'	    => 'template-parts/block/source-article.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'admin-post',
                'keywords'			    => array('titre', 'source', 'image', 'article', 'lien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'lien-externe',
                'title'				    => __('Lien externe'),
                'description'		    => __('Lien externe ( ex: bibliographie, ressources supplémentaires)'),
                'placeholder'		    => __('Lien externe'),
                'render_template'	    => 'template-parts/block/lien-externe.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'admin-links',
                'keywords'			    => array('lien', 'article', 'externe', 'titre'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'telechargement-fichier',
                'title'				    => __('Téléchargement fichier'),
                'description'		    => __('Fichier à télécharger (ex : PDF, Powerpoint …)'),
                'placeholder'		    => __('téléchargement fichier'),
                'render_template'	    => 'template-parts/block/telechargement-fichier.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'download',
                'keywords'			    => array('téléchargement', 'fichier', 'titre'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'partenaires',
                'title'				    => __('Partenaires / Membres (accueil)'),
                'description'		    => __('Liste de certains partenaires'),
                'placeholder'		    => __('Partenaires / membres (accueil)'),
                'render_template'	    => 'template-parts/block/partenaires.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('partenaires', 'titre', 'texte', 'lien', 'hyperlien'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'supports'			=> array('anchor' => true),
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'partenaires-liste-complete',
                'title'				    => __('Partenaires (tous)'),
                'description'		    => __('Liste de tous les partenaires'),
                'placeholder'		    => __('Partenaires (tous)'),
                'render_template'	    => 'template-parts/block/partenaires-liste-complete.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('partenaires', 'titre', 'texte', 'lien', 'hyperlien'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'partenaires-liste-complete-2',
                'title'				    => __('Membres (tous)'),
                'description'		    => __('Liste de tous les membres'),
                'placeholder'		    => __('Membres (tous)'),
                'render_template'	    => 'template-parts/block/partenaires-liste-complete2.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'buddicons-buddypress-logo',
                'keywords'			    => array('partenaires', 'titre', 'texte', 'lien', 'hyperlien'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                        ),
            )
        );        

        acf_register_block_type(
            array(
                'name'				    => 'membres',
                'title'				    => __('Intervenants (accueil)'),
                'description'		    => __('Liste de certains intervenants'),
                'placeholder'		    => __('Intervenants (accueil)'),
                'render_template'	    => 'template-parts/block/membres.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'groups',
                'keywords'			    => array('membres', 'titre', 'texte', 'lien', 'hyperlien'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'membres-liste-cemplete',
                'title'				    => __('Intervenants (tous)'),
                'description'		    => __('Tous les intervenants'),
                'placeholder'		    => __('Intervenants (tous)'),
                'render_template'	    => 'template-parts/block/membres-liste-complete.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'groups',
                'keywords'			    => array('membres', 'titre', 'texte', 'lien', 'hyperlien'),
                'supports'	            => array(
                                            'align'		=> false,
                                            'multiple' => false,
                                        ),
            )
        );


       

        


        
        acf_register_block_type(
            array(
                'name'				    => 'bloc-accueil',
                'title'				    => __('Accueil'),
                'description'		    => __('Bloc vidéo utilisé sur la page d\'accueil'),
                'placeholder'		    => __('Bloc accueil'),
                'render_template'	    => 'template-parts/block/bloc-accueil.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'admin-home',
                'keywords'			    => array('accueil', 'titre', 'texte', 'video'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-se-former',
                'title'				    => __('Accueil - S\'informer'),
                'description'		    => __('Met en avant les catégories d\'article "S\'informer" Bloc prévu pour la page d\'accueil'),
                'placeholder'		    => __('Accueil - se former'),
                'render_template'	    => 'template-parts/block/accueil-se-former.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'welcome-learn-more',
                'keywords'			    => array('accueil', 'titre', 'texte', 'lien', 'se-former'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );   
        
        acf_register_block_type(
            array(
                'name'				    => 'accueil-inspire',
                'title'				    => __('Accueil - S\'inspirer'),
                'description'		    => __('Met en avant les catégories d\'article "S\'inspirer" Bloc prévu pour la page d\'accueil'),
                'placeholder'		    => __('Accueil - s\'inspirer'),
                'render_template'	    => 'template-parts/block/accueil-inspire.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'lightbulb',
                'keywords'			    => array('accueil', 'titre', 'texte', 'lien', 's\'inspirer'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-boite-a-outils',
                'title'				    => __('Accueil - boite à outils'),
                'description'		    => __('Bloc Gutenberg qui permet d’afficher les 2 derniers articles “Boîte à outils” publiés. Il contient un titre H2 + contenu texte (avec l’éditeur de contenus : liens et texte en gras autorisé) + le lisitng des 2 articles “Boîtye à outils” + hyperlien (vers la page d’archive) [en option] + Libellé du lien [en option].
                Lorsque l’on clique sur l’un des articles, on atterrit sur la page correspondante'),
                'placeholder'		    => __('Accueil - boite à outils'),
                'render_template'	    => 'template-parts/block/accueil-boite-a-outils.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'archive',
                'keywords'			    => array('accueil', 'titre', 'texte', 'lien', 'boite'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-liste-inspire',
                'title'				    => __('Accueil - liste - s\'inspire'),
                'description'		    => __('Bloc Gutenberg qui permet d’afficher les derniers articles “S\'inspirer” publiés. Il contient un titre H2 + contenu texte (avec l’éditeur de contenus : liens et texte en gras autorisé) + le lisitng des 2 articles “S\'inspirer” + hyperlien (vers la page d’archive) [en option] + Libellé du lien [en option].
                Lorsque l’on clique sur l’un des articles, on atterrit sur la page correspondante'),
                'placeholder'		    => __('Accueil - liste s\'inpirer'),
                'render_template'	    => 'template-parts/block/accueil-liste-inspire.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'list-view',
                'keywords'			    => array('accueil', 'titre', 'texte', 'lien', 'inspire'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        acf_register_block_type(
            array(
                'name'				    => 'accueil-liste-informer',
                'title'				    => __('Accueil - liste - s\'informer'),
                'description'		    => __('Bloc Gutenberg qui permet d’afficher les 2 derniers articles “s\'informer” publiés. Il contient un titre H2 + contenu texte (avec l’éditeur de contenus : liens et texte en gras autorisé) + le lisitng des 2 articles “s\'informer” + hyperlien (vers la page d’archive) [en option] + Libellé du lien [en option].
                Lorsque l’on clique sur l’un des articles, on atterrit sur la page correspondante'),
                'placeholder'		    => __('Accueil - liste s\'informer'),
                'render_template'	    => 'template-parts/block/accueil-liste-informe.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'list-view',
                'keywords'			    => array('accueil', 'titre', 'texte', 'lien', 'informe'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );


        // acf_register_block_type(
        //     array(
        //         'name'				    => 'bloc-home',
        //         'title'				    => __('Logo accueil'),
        //         'description'		    => __('Utilisé sur la homepage pour afficher le logo de la Fabrique des Récits'),
        //         'placeholder'		    => __('Logo accueil'),
        //         'render_template'	    => 'template-parts/block/bloc-home.php',
        //         'category'			    => 'sparknews',
        //         'mode'                  => 'auto',
        //         'icon'				    => 'admin-home',
        //         'keywords'			    => array('accueil', 'animation', 'svg', 'logo'),
        //         'supports'	            => array(
        //                                     'align'		=> false,
        //                                     'multiple' => false,
        //                                 ),
        //     )
        // );

        acf_register_block_type(
            array(
                'name'				    => 'texte-fleche',
                'title'				    => __('Texte + flèche'),
                'description'		    => __('Texte d\'introduction sur la page d\'accueil avec une flèche décorative'),
                'placeholder'		    => __('Texte flèche'),
                'render_template'	    => 'template-parts/block/texte-fleche.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'arrow-down-alt',
                'keywords'			    => array('accueil', 'texte', 'titre', 'fleche'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'accueil-intro',
                'title'				    => __('Accueil - introduction'),
                'description'		    => __('Texte d\'introduction sur la page d\'accueil'),
                'placeholder'		    => __('Accueil - introduction'),
                'render_template'	    => 'template-parts/block/accueil-intro.php',
                'category'			    => 'sparknews',
                'mode'                  => 'edit',
                'icon'				    => 'media-text',
                'keywords'			    => array('accueil', 'texte', 'titre', 'jaune', 'introduction'),
                'supports'	            => array(
                                            'align'		=> false,
                                        ),
            )
        );

    }
}



//Intro accueil
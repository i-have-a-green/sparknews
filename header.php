<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/svg" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.svg">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<!--<link rel="manifest" href="<?php //echo get_template_directory_uri(); ?>/favicon/manifest.json">-->
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Skip Links-->
	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'sparknews' ); ?></a>
	<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'sparknews' ); ?></a>
	<a class="skip-link" tabindex="0"  href="#footer"><?php esc_html_e( 'Accéder au pied de page', 'sparknews' ); ?></a>

	<nav id="topbar" class="small-text">
		<a id="topbar-logo" href="<?php echo get_home_url(); ?>" title="<?php esc_html_e( 'Lien vers la page d\'accueil', 'sparknews') ?>">
			<?php 
			// Custom Logo
			$logo = get_field('logo', 'options');
			//$size = 'icon';
			$size = 'logo-header';
			if( $logo ) { 
				echo wp_get_attachment_image( $logo, $size );
			}
			?>
		</a>
		<div id="menu">
			<!-- Burger button -->
			<button id="burger-menu" class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'sparknews' ); ?>">
				<img alt="<?php esc_html_e( 'Menu', 'sparknews' ); ?>" aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="16" width="30">
			</button>

			<!-- Close Button -->
			<div id="close-menu">
				<button class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ihag' ); ?>">
					<img alt="<?php esc_html_e( 'Fermer', 'sparknews' ); ?>" aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="36" width="36">
				</button>
			</div>
			
			<!-- Menu -->
			<?php echo ihag_menu('primary'); ?>

		</div>
	</nav>

	<!-- #content -->
	<div id="content">

<?php

include_once("inc/acf.php");
include_once("inc/acf-block.php");
include_once("inc/settings-gutenberg.php");
include_once("inc/widget.php");
include_once("inc/clean.php");
include_once("inc/no-comment.php");
include_once("inc/images.php");
include_once("inc/custom-post-type.php");
include_once("inc/breadcrumb.php");
include_once("inc/menu.php");
include_once("inc/enqueue_scripts.php");
include_once("inc/contact.php");
include_once("inc/modale.php");



// Adding excerpt for page
//add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
//add_filter( 'excerpt_length', function( $length ) { return 20; } );

function ihag_unregister_taxonomy(){
    register_taxonomy('post_tag', array());
    register_taxonomy('category', array());
	// add_post_type_support( 'page', 'excerpt' );
	// remove_post_type_support( 'page', 'thumbnail' );
}
add_action('init', 'ihag_unregister_taxonomy');

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
    if ( isset( $_GET['from_post'] ) ) {
        $my_post = get_post( $_GET['from_post'] );
        if ( $my_post )
            return $my_post->post_content;
    }
    return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts($query) {

  if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
      $query->set('post_type', array("post", "rendez-vous", "solution"));
      $query->set('posts_per_page', -1);
  } 

}
//add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL($url)  {
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}


// 
function ihag_get_term($post, $taxonomy){
  if ( class_exists('WPSEO_Primary_Term') ):
	$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, get_the_id( $post ) );
	$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
  $term = get_term( $wpseo_primary_term );
  if ( is_wp_error( $term ) ) {
    $term = get_the_terms($post, $taxonomy);
    return $term[0];
  }
  return $term;
 else:
 	$term = get_the_terms($post, $taxonomy);
	return $term[0];
 endif;
}


function target_main_category_query_with_conditional_tags( $query ) {
  if ( ! is_admin() && $query->is_main_query() ) {
      // Not a query for an admin page.
      // It's the main query for a front end page of your site.
      if ( is_tax('taxo_training') && isset($_GET['var_taxo_tag']) && !empty($_GET['var_taxo_tag']) ) {
          // It's the main query for a category archive.
          // Let's change the query for category archives.
         $query->set( 'tax_query', array(
          array(
            'taxonomy' => 'taxo_tag',
            'field'    => 'term_id',
            'terms'    => $_GET['var_taxo_tag'],
          )));
        }
  }
}
add_action( 'pre_get_posts', 'target_main_category_query_with_conditional_tags' );


// fonction scroll
function ihag_scroll(WP_REST_Request $request){

  $offset = ( (int)sanitize_text_field( $_POST["page"] ) - 1) * get_option('posts_per_page' );
  $args = array(
    'posts_per_page' => get_option('posts_per_page' ),
    'post_type'   => $_POST['cpt'],
    'post_status' => 'publish',
    'offset'  => $offset,
  );


  if(isset($_POST["taxo"]) && !empty($_POST["taxo"]) && $_POST["taxo"] > 0 ){
    $args['tax_query'] = array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'taxo_'.$_POST['cpt'],
        'field'    => 'term_id',
        'terms'    => (int) sanitize_text_field($_POST["taxo"])
      ),
    );
  }
  if(isset($_POST["taxo_tag_var"]) && !empty($_POST["taxo_tag_var"]) && $_POST["taxo_tag_var"] > 0 ){
    if($_POST['cpt'] == 'inspire'){
      $args['tax_query']  = array(
        array(
          'taxonomy' => 'taxo_tag_inspire', 
          'field'    => 'term_id',
          'terms'    => (int) sanitize_text_field($_POST["taxo_tag_var"])
        )
      );
    }
    else{
      $args['tax_query']  = array(
        array(
          'taxonomy' => 'taxo_tag',
          'field'    => 'term_id',
          'terms'    => (int) sanitize_text_field($_POST["taxo_tag_var"])
        )
      );
    }
    
  
  }
  $custom_query = new WP_Query($args);
  if ( $custom_query->have_posts() ) : 
    while ( $custom_query->have_posts() ) : 
        $custom_query->the_post();
		    get_template_part( 'template-parts/archive', get_post_type() );
    endwhile;
  endif;

  return new WP_REST_Response( NULL, 200 );
}


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'sparknews', 'scroll',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_scroll',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

/*recupère l'url d'une video */
function retrieve_id_video($video){

  preg_match('/src="(.+?)"/', $video, $matches_url );
  $src = $matches_url[1];	

  preg_match('/embed(.*?)?feature/', $src, $matches_id );
  $id = $matches_id[1];
  $id = str_replace( str_split( '?/' ), '', $id );

  return $id;

}


function lettrine($letterineTerm){
    $show_letterine = get_field('show_letterine', $letterineTerm);
    $letterine = get_field('letterine', $letterineTerm);

    if ( ($show_letterine) ) {
      echo '<span class="icon-font lettrine" aria-hidden="true">'.$letterine.'</span>';
    }
}

<div class="member-single">
    <div class="member-thumbnail brand-black-bg image-container">
        <?php 
        if ( has_post_thumbnail() ) {
            echo get_the_post_thumbnail($post, 'member');
        } else {
            $image = get_field('imageFallback', 'option');
            echo wp_get_attachment_image( $image, 'member' );
        }
        ?>
    </div>

    <h3 class="member-name"><?php echo get_the_title($post); ?></h3>
    <p><?php the_field( "description", $post ); ?></p>
</div>

<aside id="sticky-content" class="wrapper">

    <?php 
    $source_name = get_field('source_name');
    $source_url = get_field('source_link');

    if ( $source_name && $source_url ) {
        echo '<div>';
            echo '<p class="h3-like white" >'; 
                esc_html_e('Article issu de', 'sparknews');
            echo ' '.$source_name.'</p>';
            echo '<div class="custom-color">';
            echo '<a class="link-default h3-like" href="'.$source_url.'">';
                esc_html_e('→ Voir l’article original', 'sparknews');
            echo '</a>';
            echo '</div>';
        echo '</div>';
        echo '<br>';
        echo '<br>';
    }

    ?>

    <?php
    // Terms : Category + Type + Keywords
    $term = get_the_terms($post, 'taxo_'.get_post_type());

    // Category
    if($term):

    echo '<div class="article-info-list custom-color">';

        foreach ($term as $row):
         
            echo '<a class="button link-discrete" href="'. get_category_link($row) .'">';
            lettrine($row);
            echo $row->name;
            echo '</a>';
            
        endforeach;

    echo '</div>';
    //echo '<br>';
        
    endif; 


    echo '<div class="article-info-list custom-color">';

    // Keywords - For Training post only
    if(get_post_type() == "training") {

        $all_keyWords = get_the_terms($post, "taxo_tag");

        if($all_keyWords):
    
            foreach ($all_keyWords as $row):?>
                <a class="button" href="<?php echo get_category_link($row);?>">#<?php echo $row->name; ?></a>
            <?php endforeach;
    
        endif; 

    }

    /*
    // Type of article (training)
    if(get_post_type() == "training"):
        $ref = ihag_get_term($post, 'taxo_reference');
        echo '<p class="button small-text button-type">'. $ref->name .'</p>';
    endif;
    */

    echo '</div>';
    echo '<br>';
    ?>

    <!-- Share -->
    <div class="post-share" class="list title-font">
        <p class="h3-like">
            <span class="lettrine icon-font">*</span>
            <?php esc_html_e('Partager', 'sparknews')?>
        </p>

        <a class="JSrslink custom-color" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Facebook', 'sparknews')?>">
           <p>&rarr; <?php esc_html_e('Facebook', 'sparknews') ?></p>
        </a>

        <a class="JSrslink custom-color" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Linkedin', 'sparknews')?>">
            <p>&rarr; <?php esc_html_e('Linkedin', 'sparknews') ?></p>
        </a>

        <a class="JSrslink custom-color" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>" title="<?php esc_html_e('Partager l\'article sur Twitter', 'sparknews')?>">
            <p>&rarr; <?php esc_html_e('Twitter', 'sparknews') ?></p>
        </a>
            
        <div class="copyLink custom-color"> 
            <input type="text" class="copyLinkValue" value="<?php echo get_the_permalink();?>">
            <p>&rarr; <?php esc_html_e('Copier l\'url', 'sparknews') ?></p>
            <div class="copyLinkMessage link-default">
                <?php esc_html_e('Lien copié dans le presse-papier !', 'sparknews')?>
            </div>
        </div>
    </div>

    <?php 
    // Date
    $hide_date = get_field('hide_publication');
    if (!$hide_date) { ?>
        <br>
        <p class="title-font">
            <?php esc_html_e('@ Publié le ', 'sparknews')?>
            <time><?php echo get_the_date();?></time>
        </p>
    <?php
    }?>

</aside>
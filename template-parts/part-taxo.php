<?php
/**
 * Template part for displaying page part in taxo.php and archive.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<div class="term-listing margin-b">

	<!-- Link to category-->
	<?php if($cpt == 'inspire'){
		$terms = get_terms( 
			array(
				'taxonomy' => 'taxo_inspire',
				//'hide_empty' => false,
			) 
		);

	} else { 
		$terms = get_terms( 
			array(
				'taxonomy' => 'taxo_'.$cpt,
				//'hide_empty' => false,
			) 
		);
	}

	echo '<div class="list">';

	if($terms):

		// For Article Training 
		if ($cpt == 'training' || $cpt == 'inspire'):

			// Show the first category 
			foreach ($terms as $row):

				$first_cat = false ;
				if ( (get_field("first_cat_training", $row)) == true ) {
					$first_cat = true;
				}

				if ($first_cat) {
					echo '<a class="button button-color-bg" href="'. get_term_link($row) .'">';
					echo lettrine($row);
					echo $row->name;
					echo '</a>';
				}

			endforeach; 

			// Show the other categories 
			foreach ($terms as $row):

				//var_dump($row);

				$first_cat = false ;
				if ( (get_field("first_cat_training", $row)) == true ) {
					$first_cat = true;
				}

				if (!$first_cat) {
					echo '<a class="button button-color-bg" href="'. get_term_link($row) .'">';
					echo lettrine($row);
					echo $row->name;
					echo '</a>';
				}

			endforeach; 

		// For Article "Blog" and Article Tools 
		else :

		foreach ($terms as $row):
			echo '<a class="button button-color-bg" href="'. get_term_link($row) .'">';
				lettrine($row);
				echo $row->name;
			echo '</a>';
			echo '<br>';
		endforeach; 
		
		endif;
			
	endif; ?>

		<!-- Link to archive -->
		<a class="button button-color-bg" href="<?php echo get_the_permalink(get_field('archive_'.$cpt, 'option')); ?>">
			&rarr; <?php esc_html_e('Voir tout', 'sparknews');?>
		</a>

	<?php echo '</div>'; ?>

</div>

<!-- CPT training -->
<?php if ($cpt == 'training'): ?>

	<!-- Select-->
	<?php if( $terms = get_terms( array(
		'taxonomy' => 'taxo_tag', // to make it simple I use default categories
		//'hide_empty' => false,
		) ) ) : 
		// if categories exist, display the dropdown
		if( is_tax( "taxo_training" ) ){
			$permalien = get_category_link(get_queried_object()->term_id);
			// var_dump($permalien);
		} else {
			$permalien = get_the_permalink();
		}		
	?>
	<div id="select-archive-wrapper">
		<select id="select-archive" name="categoryfilter" onchange="location.href='<?php echo $permalien;?>?var_taxo_tag='+this.value+''">
			<option value=""><?php esc_html_e('Filtre', 'sparknews');?></option>
			<?php
				var_dump($permalien);
			foreach ( $terms as $term ) :
				$selected = '';
				if($term->term_id == $_GET['var_taxo_tag']): 
					$selected = 'selected';
				endif;
				echo '<option value="' . $term->term_id . '" '.$selected.' >' . $term->name . '</option>'; // ID of the category as an option value
			endforeach;
			?>
		</select>
	</div>		
<?php endif; ?>

<!-- End of Select -->
<?php endif; ?>

<!-- CPT inspire -->
<?php if ($cpt == 'inspire'): ?>

	<!-- Select-->
	<?php if( $terms = get_terms( array(
		'taxonomy' => 'taxo_tag_inspire', // to make it simple I use default categories
		//'hide_empty' => false,
		) ) ) : 
		// if categories exist, display the dropdown
		if( is_tax( "taxo_inspire" ) ){
			$permalien = get_category_link(get_queried_object()->term_id);
			// var_dump($permalien);
		} else {
			$permalien = get_the_permalink();
		}		
	?>

	<div id="select-archive-wrapper">
		<select id="select-archive" name="categoryfilter" onchange="location.href='<?php echo $permalien;?>?var_taxo_tag='+this.value+''">
			<option value=""><?php esc_html_e('Filtre', 'sparknews');?></option>
			<?php
				var_dump($permalien);
			foreach ( $terms as $term ) :
				$selected = '';
				if($term->term_id == $_GET['var_taxo_tag']): 
					$selected = 'selected';
				endif;
				echo '<option value="' . $term->term_id . '" '.$selected.' >' . $term->name . '</option>'; // ID of the category as an option value
			endforeach;
			?>
		</select>
	</div>
<?php endif; ?>

<!-- End of Select -->
<?php endif; ?>
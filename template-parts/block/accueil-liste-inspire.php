<?php
/**
 * Block Name: bloc accueil
 */
 ?>

<section class="blk-tools v-padding-regular no-useless-margin brand-yellow-bg inspire">

<?php

if ( empty(get_field('title')) ):?>

	<em>Renseigner le bloc</em>
		
<?php else :?>

	<div class="responsive-text-container no-useless-margin custom-wrapper">

		<div>

			<?php 
			// Title with icon (Stratus)
			$title = get_field('title');
			if($title) {

				$icon = get_field('activate_icon');
				if ( $icon ) {

					$style = get_field('icon_style');
					if (!$style) {
						$style = 'style_1';
						// If no style selected, use style_1 by default
					}

					echo '<h2 class="h1-like brand-black title_with_icon '. $style .'">'. $title .'</h2>';

				} else {
					echo '<h2 class="h1-like brand-black">'. $title .'</h2>';
				}
				
			} ?>

		</div>

		<?php if(!empty(get_field('text'))):?>
			<div class="entry-content"><?php echo get_field('text'); ?>
		</div>
		<?php endif; ?>
		
	</div>

	<!-- Listing Tools Archive -->
	<div class="listing-archive">
		<?php

		$arg = array(
			'posts_per_page' => 6,
			'orderby'        => 'rand',
			'include'          => get_field('inspire'),
			'post_status' 	 => 'publish',
			'post_type'		 => 'inspire',
			);
			// var_dump($arg);

		$posts = get_posts( $arg );

		// var_dump($posts );
		global $post;

		foreach($posts as $post): 
			
			setup_postdata( $post );
			
			get_template_part( 'template-parts/standard', get_post_type() );

		endforeach; 

		wp_reset_postdata();
		
		?>
	</div><!-- End of Lisitng -->

	<?php 
			if(!empty(get_field('link'))):
				$link = get_field('link');
				if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button-dot center" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php _e('Voir plus !', 'sparknews'); ?></a>
				<?php endif; 
			endif; 
			?>

<?php endif; ?>

</section>


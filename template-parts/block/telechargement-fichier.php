<?php
/**
 * Block Name: téléchargement fichier
 */
 ?>

<section class="blk-download wrapper v-padding-small">

<?php
$file = get_field('download_file');

if ( empty($file) ):?>

		<em>Renseigner le bloc</em>

<?php else :?>

	<div class="card link-discrete" href="<?php echo $link_url ?>" title="<?php esc_html_e('Consulter le lien externe : ', 'sparknews'); echo $link_title; ?>" target="<?php echo esc_attr( $link_target ); ?>">

		<div class="line brd-top-dot brd-btm-dot">
			<div class="icon-container h1-like white-bg">
			<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/download.svg" height="20" width="20">
			</div>
			
			<div class="no-useless-margin">
				<?php if( !empty(get_field('title')) ):?>
					<p class="h1-like custom-color"><?php the_field('title');?></p><br>
				<?php endif; ?>

				<div class="line">
					<p><?php echo $file['filename']?></p><!-- supprime tout jusqu'au dernier point +1 -->
					&nbsp;&nbsp;&nbsp;&nbsp;
					<p><?php echo round(($file['filesize']/1000000),1);?>&nbsp;Mo</p>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<p><?php esc_html_e('Fichier ', 'sparknews'); ?><?php echo substr($file['filename'], strrpos($file['filename'], '.') + 1); ?></p>
				</div>

				<a class="button custom-color" href="<?php echo $file['url']; ?>" download><?php esc_html_e('Télécharger', 'sparknews'); ?></a>
			</div>
		</div>

	</div>

<?php endif; ?>

</section>



<?php
/**
 * Block Name: bloc accueil s'informer
 */
 ?>

<?php 
$image = get_field('image');
?>

<section class="blk-learn narrow-wrapper v-padding-regular brand-orange-bg no-useless-margin <?php if( $image ) { echo 'has-image';} ?>">

<?php

if ( empty(get_field('title')) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<?php
	if( $image ) {
		echo '<div class="image-container">';
		echo wp_get_attachment_image($image, 'medium');			
		echo '</div>';
	} 
	?>

	<?php 
	// Title
	$title = get_field('title');
    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="h1-like margin-b title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
            echo '<h2 class="h1-like margin-b">'. $title .'</h2>';
		}
		
    } ?>


	<?php if(get_field('description')):?>
		<div class="entry-content"><?php echo get_field('description'); ?></div>
	<?php endif; ?>

	<?php 
	$terms = get_terms(

		array(
			'taxonomy' => 'taxo_training',
		) 
	);
	if($terms): 
		echo '<nav class="term-listing">';

			foreach ($terms as $row):
			
				$first_cat = false ;
				if ( (get_field("first_cat_training", $row)) == true ) {
					$first_cat = true;
				}

				if ($first_cat) {
					echo '<a class="button small-text button-color-bg" href="'. get_term_link($row) .'">';
					echo lettrine($row);
					echo $row->name;
					echo '</a>';
				}

			endforeach;

			foreach ($terms as $row):
		
				
				$first_cat = false ;
				$last_cat = false ;
				if ( (get_field("first_cat_training", $row)) == true ) {
					$first_cat = true;
				}
				if ( (get_field("last_cat_training", $row)) == true ) {
					$last_cat = true;
				}

				if (!$last_cat && !$first_cat) {
					echo '<a class="button small-text button-color-bg" href="'. get_term_link($row) .'">';
					echo lettrine($row);
					echo $row->name;
					echo '</a>';
				}

			endforeach;
			
			foreach ($terms as $row):

					$last_cat = false ;
					if ( (get_field("last_cat_training", $row)) == true ) {
						$last_cat = true;
					}

					if ($last_cat) {
						echo '<a class="button small-text button-color-bg last-cat" href="'. get_term_link($row) .'">';
						echo lettrine($row);
						echo $row->name;
						echo '</a>';
					}
			endforeach;

		echo '</nav>';
	endif;
	?>


	<?php 
	if(!empty(get_field('link'))):
		$link = get_field('link');
		if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button-dot button-color-bg h3-like" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; 
	endif; 
	?>

<?php endif; ?>

</section>


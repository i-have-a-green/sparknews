<?php
/**
 * Block Name: Accueil - Intro jaune
 */
 ?>

<?php
$image = get_field('image');
 ?>

<section class="<?php if ($image) { echo 'blk-text-yellow-with-image';} else { echo 'blk-text-yellow';} ?> brand-yellow-bg narrow-wrapper v-padding-regular no-useless-margin">

<?php
$texte = get_field('text');

if ( empty($texte) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<?php
		if ($image) {
			echo '<div class="image-container">';
			echo wp_get_attachment_image($image, 'medium');
			echo '</div>';
		}
	?>

	<?php 
	// Title with icon (Stratus)
	$title = get_field('title');
	if( $title ) {

		$icon = get_field('activate_icon');
		if ( $icon ) {

			$style = get_field('icon_style');
			if (!$style) {
				$style = 'style_1'; // If no style selected, use style_1 by default
			}

			echo '<h2 class="h1-like title_with_icon '. $style .'">'. $title .'</h2>';

		} else {
			echo '<h2 class="h1-like">'. $title .'</h2>';
		}
	} ?>

	<div class="entry-content">
		<?php the_field('text');?>

		<?php 
		$link = get_field('link');
		$link2 = get_field('link_2');

		if( $link || $link2 ) {

			echo '<div class="btn-container">';

				// Link 1 : optional
				$link = get_field('link');
				if( $link ){

					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button-dot button-color-bg" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php
				}

				// Link 2: optional
				$link2 = get_field('link_2');
				if( $link2 ){

					$link_url2 = $link2['url'];
					$link_title2 = $link2['title'];
					$link_target2 = $link2['target'] ? $link2['target'] : '_self';
					?>
					<a class="button-dot button-color-bg" href="<?php echo esc_url( $link_url2 ); ?>" target="<?php echo esc_attr( $link_target2 ); ?>"><?php echo esc_html( $link_title2 ); ?></a>
					<?php 
				}
				

			echo '</div>';

		}
		?>
		
	</div>

<?php endif; ?>

</section>

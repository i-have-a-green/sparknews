<?php
/**
 * Block Name: Texte + Flèche
 */
?>

<?php
$title = get_field('title');
$texte = get_field('text');
?>

<section class="blk-text-fleche narrow-wrapper no-useless-margin <?php if ($texte) {echo 'has-content'; } ?>">

<?php
if ( empty($title) ):?>
		<em class="center">Renseigner le bloc</em>
<?php else :?>

	<?php // Title
	$title = get_field('title');
	
    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="brand-orange giant-title title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
            echo '<h2 class="brand-orange giant-title">'. $title .'</h2>';
		}
		
    } ?>


	<?php 
	if ($texte) {
		echo '<div class="entry-content">'. $texte .'</div>';
	} ?>

	<div class="giant-icon icon-font" aria-hidden="true">&darr;</div>



<?php endif; ?>

</section>

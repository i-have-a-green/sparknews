<?php
/**
 * Block Name: Partenaires / Membres
 */
 ?>

<?php
$title = get_field('title');
$text = get_field('text');
?>

<section id="<?php echo $block['id']; ?>" class="blk-partner wrapper no-useless-margin white brand-blue-bg <?php if( $title ) { echo 'has-title';} ?>">

<?php
$content = get_field('partners');
if ( empty($content) ):?>
    <em>Renseigner le bloc</em>
<?php else :?>

    <?php // Title
    $title = get_field('title');

    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="h1-like title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
            echo '<h2 class="h1-like">'. $title .'</h2>';
        }

    }?>
    
    <div class="text-container">
        <?php if(!empty($text)):?>
            <div class="entry-content"><?php echo $text; ?></div>
        <?php endif; ?>

        <?php
        // Link CTA (optionel)
        $cta = get_field('cta');
        if( $cta ): 
            $cta_url = $cta['url'];
            $cta_title = $cta['title'];
            $cta_target = $cta['target'] ? $cta['target'] : '_self';
            ?>
            <a class="button-dot button-color-bg" href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo esc_attr( $cta_target ); ?>"><?php echo esc_html( $cta_title ); ?></a>
        <?php endif; ?>

        <?php
        // Link Archive (-> list of all the members)
        $show_archive_link_1 = get_field('show_archive_link_1');
        $show_archive_link_2 = get_field('show_archive_link_2');
        $archive_page_1 = get_field('list_partner_1', 'option');
        $archive_page_2 = get_field('list_partner_2', 'option');
        
        if ($show_archive_link_1 && $archive_page_1 ) {
            echo '<a class="button-dot button-color-bg" href="'. esc_url($archive_page_1) .'#partnerListing1">';

                if ( get_field('archive_text_1') ) {
                    the_field('archive_text_1');
                } else {
                    esc_html_e('Découvrir tous les partenaires', 'sparknews');
                }

            echo '</a>';
        }

        if ($show_archive_link_2 && $archive_page_2 ) {
            echo '<a class="button-dot button-color-bg" href="'. esc_url($archive_page_2) .'#partnerListing2">';
            
                if ( get_field('archive_text_2') ) {
                    the_field('archive_text_2');
                } else {
                    esc_html_e('Découvrir tous les membres', 'sparknews');
                }

            echo '</a>';
        }
        ?>

    </div>

    <div class="basic-listing-partner">
        
    <?php // Check rows exists.
        if( have_rows('partners') ):

            // Loop through rows.
            while( have_rows('partners') ) : the_row();

                // Load sub field value.
                $objet_partner = get_sub_field('partner');
                $link = get_field( "link", $objet_partner->ID );
                ?>

                <div class="partner-single image-container">

                    <?php if ($link) {
                        echo '<a class="image-container" href="'.$link.'" target="_blank">';
                    } 
                        // Partner Thumbnail
                        echo get_the_post_thumbnail( $objet_partner, 'partner' );

                    if ($link) {
                        echo "</a>";
                    } ?>

                </div>

            <?php
            // End loop.
            endwhile;

        // No value.
        else :
            // Do something...
        endif;?>

    </div><!-- /basic-listing -->       
	
<?php endif; ?>

</section>


<?php
/*
 * Block Name: Titre + texte + image
 */
?>

<?php
$image = get_field('image');
$title = get_field('title');
$text = get_field('text');
?>

<section class="blk-text-img narrow-wrapper v-padding-regular no-useless-margin <?php if($text) {echo 'has-content';}?>">

<?php

if ( empty($text) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<?php
    if($title) {
        // Title with icon (Stratus)
        $icon = get_field('activate_icon');
        if ( $icon ) {
            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }
            echo '<h2 class="h1-like custom-color title_with_icon '. $style .'">'. $title .'</h2>';
        } else {
			echo '<h2 class="h1-like custom-color">'. $title .'</h2>';
        }
    }?>


	<?php 
	if($text) {
		echo '<div class="entry-content">'. $text .'</div>';
	}?>

	<?php 
	$link = get_field('link');
	if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<a class="button-dot h3-like" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
	<?php endif; ?>
	
	<div class="image-container <?php if (get_field('image_color')) { echo 'no-filter';} ?>">
		<?php 
		$symbol = get_field('show_symbol'); 
		$symbol_content = get_field('symbol_content'); 

		if(!$symbol) {
			// Pas de symbole : on affiche une image
			echo wp_get_attachment_image($image, 'free-height');
			$legend = get_field('legende');
				// Légende de l'image
				if ($legend) {
					echo '<div class="entry-content small-text">'. $legend .'</div>' ;
				}
		} else {
			echo '<div class="giant-icon icon-font">'.$symbol_content.'</div>';
		}
		?>
	</div>
	
<?php endif; ?>

</section>


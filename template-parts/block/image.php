<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<section class="blk-img wrapper v-padding-small wrapper">

<?php
$image = get_field('image');

if ( !$image ):?>
	<em>Renseigner l'image</em>
	
<?php else :?>

	<?php 
	$size = 'free-height';
	echo wp_get_attachment_image($image, $size); 
	
	$legend = get_field('legende');
		if ($legend) {
			echo '<div class="entry-content small-text">'. $legend .'</div>' ;
		}
	?>
	
<?php endif; ?>
</section>

<?php
/**
 * Block Name: source article
 */
 ?>

 <section class="blk-source wrapper v-padding-small">

<?php
$post_title = get_field('post_title');
$image = get_field('image');
$link =  get_field('link');

if ( empty($post_title) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<div>
		<i class="body-like"><?php esc_html_e('Article issus de :', 'sparknews')?></i>
		<p class="h1-like custom-color"><?php the_field('author');?></p>
		<i class="body-like"><?php esc_html_e('Voir l\'article original :', 'sparknews')?></i>
	</div>

	<a class="card link-discrete" href="<?php echo $link ?>" title="<?php esc_html_e('Voir la source de l\'article', 'sparknews')?>" >

		<div class="brd-btm-dot brd-top-dot">
			<?php if($image) {
				// If Image
				echo'<div class="image-source">';
				wp_get_attachment_image($image, 'medium');
				echo '</div>';
			} ?>

			<!-- Text -->
			<div class="no-useless-margin">
				<h2 class="h2-like custom-color"><?php echo $post_title;?></h2>
				<?php if(!empty(get_field('link'))):?>
					<p class="link-default card-url"><?php echo get_field('link')?></p>
				<?php endif; ?>
			</div>
		</div>

	</a>
<?php endif; ?>

</section>


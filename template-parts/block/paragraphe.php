<?php
/**
 * Block Name: Paragraphe
 */
 ?>

<?php
$texte = get_field('text');
$random = get_field('random_color');

if ( empty($texte) ):?>

	<em>Renseigner le paragraphe</em>

<?php else :?>

	<div class="entry-content wrapper title-custom-color <?php if($random) {echo 'custom-color';} ?>">
		<?php echo $texte; ?>
	</div>

<?php endif; ?>
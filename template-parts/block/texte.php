<?php
/**
 * Block Name: Texte
 */
 ?>

<?php
$image = get_field('image');
 ?>

<section class="wrapper v-padding-regular no-useless-margin">

<?php
$texte = get_field('text');

if ( empty($texte) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<?php // Title
    $title = get_field('title');

    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="h1-like custom-color title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
			echo '<h2 class="h1-like custom-color">'. $title .'</h2>';
        }

    }?>


	<?php 
	$link = get_field('link');
	if( $link ) {
		echo '<div class="responsive-text-container">';
	}
	?>

	<div class="entry-content">
		<?php the_field('text');?>
	</div>

	<?php 
	if( $link ) {
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
		echo '<a class="button-dot" href="'. esc_url( $link_url ).'" target="'. esc_attr( $link_target ).'">'. esc_html( $link_title ).'</a>';
		echo '</div>'; // end of .responsive-text-container
	}
	?>

<?php endif; ?>

</section>

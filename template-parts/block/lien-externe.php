<?php
/**
 * Block Name: extern link
 */
 ?>

<section class="blk-external wrapper v-padding-small">

<?php
$link = get_field('link');

if ( empty($link) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>


	<?php
	if ($link) {
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	}
	?>

	<a class="card link-discrete" href="<?php echo $link_url ?>" title="<?php esc_html_e('Consulter le lien externe : ', 'sparknews'); echo $link_title; ?>" target="<?php echo esc_attr( $link_target ); ?>">

		<div class="line brd-top-dot">
			<div class="h1-like icon-container white-bg">
				<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/earth.svg" height="24" width="24">
			</div>
			
			<?php if( !empty(get_field('title')) ):?>
				<p class="h1-like custom-color"><?php the_field('title');?></p><br>
			<?php endif; ?>
		</div>

		<div class="no-useless-margin brd-btm-dot">
			<p class="underline link-default card-url"><?php echo $link_url ?></p><br>
			<p class="button custom-color">
				<?php 
				if ( $link_title ) {
					echo esc_html( $link_title );
				} else {
					// Fallback Link
					esc_html_e('Voir le site', 'sparknews');
				}?>
			</p>
		</div>
	</a>


<?php endif; ?>

</section>


<?php
/**
 * Block Name: bloc accueil vidéo
 */
 ?>

<?php $block_uniq_id = "id_".uniqid(); ?>

<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-header-home-video white primary-bg">

<?php
$description = get_field('description');

if ( empty($description) ):?>
		<em>Renseigner le bloc</em>
<?php else :?>

	<!-- wrapper -->
	<div class="wrapper v-padding-regular white">


		<?php if(!empty(get_field('title'))):?>
			<h1><?php the_field('title');?></h1>
		<?php endif; ?>


		<?php if(!empty($description)):?>
			<div class="entry-content lead-paragraph"><?php echo $description; ?></div>
		<?php endif; ?>

		<?php 
		$link = get_field('link');
		if( $link ): 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			?>
		 	<a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
		 		<?php echo esc_html( $link_title );?>
		 	</a>
		 <?php endif; ?>

		<!-- Video -->

		<div class="embed-container">

			<?php
			$video = get_field('link_video');
			if ( !$video ) :?>
				<em>Renseigner le bloc</em>
				
			<?php else :?>

					<div class="embed-container">

						<?php 
						// Test if image doesn't exists
						if (!get_field('image')) {
						?>
						<a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
						<script> 
							if (typeof iframe === 'undefined') {
								var iframe = new Object();
							}
							iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
						</script>

							<?php
								$video = get_field( 'link_video' );
								$id = retrieve_id_video($video);
							?>

							<img src="http://img.youtube.com/vi/<?php echo $id; ?>/mqdefault.jpg">

						</a> 

						<?php } else { ?>

							<a href="#" class="btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>">
							<script> 
							if (typeof iframe === 'undefined') {
								var iframe = new Object();
							}
							iframe.<?php echo $block_uniq_id;?> = '<?php echo get_field('link_video'); ?>'; 
						</script>

							<?php
							$videoThumbnail = get_field('image');
							$size = 'medium';
							echo wp_get_attachment_image($videoThumbnail, $size);?>
							</a>
						<?php } ?>
						<?php endif; ?>
		</div>

		<!-- end video -->

	</div><!--  /wrapper -->

<?php endif; ?>

</section>



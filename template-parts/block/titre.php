<?php
/**
 * Block Name: Titre
 */
 ?>

<?php
$title = get_field('title');
$html = get_field('level_title');

if ( empty($title) ):?>
	<em>Renseigner le titre</em>
<?php else :

	// Title with icon (Stratus)
	$icon = get_field('activate_icon');

	if ( $icon ) {

		$style = get_field('icon_style');
		if (!$style) {
			$style = 'style_1'; // If no style selected, use style_1 by default
		}
		
		echo "<". $html ." class='wrapper custom-title custom-color title_with_icon ". $style ."'>". $title ."</". $html .">";

	} else {

		echo "<". $html ." class='wrapper custom-title custom-color'>". $title ."</". $html .">";

	}

endif; ?>

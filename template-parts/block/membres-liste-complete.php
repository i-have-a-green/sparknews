<?php
/**
 * Block Name: Intervenants (Tous)
 */
 ?>

<?php
$title = get_field('title');
$text = get_field('text');
?>

<section id="memberListing" class="blk-member wrapper no-useless-margin white brand-blue-bg <?php if( $title ) { echo 'has-title';} ?>">

<?php
if ( empty($title) ):?>
    <em>Renseigner le bloc</em>
<?php else :?>

    <?php // Title
    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="h1-like title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
            echo '<h2 class="h1-like">'. $title .'</h2>';
        }

    }?>


    <div class="text-container">
        <?php if(!empty($text)):?>
            <div class="entry-content"><?php echo $text; ?></div>
        <?php endif; ?>

        <?php
        // Link CTA (optionel)
        $cta = get_field('cta');
        if( $cta ): 
            $cta_url = $cta['url'];
            $cta_title = $cta['title'];
            $cta_target = $cta['target'] ? $cta['target'] : '_self';
            ?>
            <a class="button small-text" href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo esc_attr( $cta_target ); ?>"><?php echo esc_html( $cta_title ); ?></a>
        <?php endif; ?>
    </div>
    
    <!-- Listing Members -->
    <div class="basic-listing-member">

		<?php
		$arg = array(
			//'orderby'        => 'rand',
            'post_type'		 => 'members',
            'post_status' 	 => 'publish',
            'posts_per_page' =>  -1,
			);

		$posts = get_posts( $arg );

		// var_dump($posts );
		global $post;

		foreach($posts as $post): 
			
            get_template_part( 'template-parts/archive-member');

		endforeach; 

		wp_reset_postdata();
		?>

    </div><!-- /basic-listing -->       
	
<?php endif; ?>

</section>


<?php
/**
 * Block Name: event
 */
 ?>

<?php
$hook = get_field('hook');
$title = get_field('title');
$image = get_field('image');
?>

<section class="blk-event v-padding-regular narrow-wrapper no-useless-margin <?php if ($image) { echo 'has-image';} ?>">

<?php
if ( empty($hook) || empty($title) ):?>

	<em>Renseigner le bloc</em>

<?php else :?>

		<?php 
		$legend = get_field('legende');
		if( $image ) {
			echo '<div class="image-container">';
			echo wp_get_attachment_image($image, 'free-height'); 

				// Légende de l'image
				if ($legend) {
					echo '<div class="entry-content small-text">'. $legend .'</div>' ;
				}
				
			echo '</div>';
			echo '<div class="text-container">';
		} ?>
		
		<?php
			// Title with icon (Stratus)
			$icon = get_field('activate_icon');
			if ( $icon ) {

				$style = get_field('icon_style');
				if (!$style) {
					$style = 'style_1'; // If no style selected, use style_1 by default
				}

				echo '<h3 class="h1-like brand-pink title_with_icon '. $style .'">'. $hook .'</h3>';

			} else {
				echo '<h3 class="h1-like brand-pink">'. $hook .'</h3>';
			}
		?>

		<?php 
		$title = get_field('title');
		if( $title ) {
			echo '<h2 class="h1-like">'. $title .'</h2>';
		} ?>


		<div class="text-container">

			<!-- Event infos -->
			<div class="event-info">
				<div class="brd-btm-dot">
					<?php 
					// Time
					if ( !empty(get_field('date_event')) || !empty(get_field('hour')) ) {
						echo '<time class="body-like">';
					}?>

						<?php if(!empty(get_field('date_event'))):?>
							<!-- Time : Day -->
							<?php // Load field value and convert to numeric timestamp.
							$unixtimestamp = strtotime( get_field('date_event') );
							// Display date in the format "l d F, Y".
							echo date_i18n( "j F Y", $unixtimestamp );
							?>
						<?php endif; ?>

						<?php if(!empty(get_field('hour'))):?>
							<!-- Time : Hour -->
							<?php the_field('hour');?>
						<?php endif; ?>

					<?php 
					// End of Time
					if ( !empty(get_field('date_event')) || !empty(get_field('hour')) ) {
						echo '</time>';
					}

					// If Time + place , add a separator :
					if( ( get_field('date_event') || get_field('hour') ) && (get_field('location')) ) {
						echo ' &mdash; ';
					} 
					?> 
					
					<?php 
					// Place
					if(get_field('location')):?>

						<i class="body-like"><?php the_field('location');?></i>

					<?php endif; ?>

				</div>
			</div>

			<!-- Text -->
			<?php if(!empty(get_field('text'))):?>
				<div class="entry-content"><?php the_field('text');?></div>
			<?php endif; ?>

		</div>
		


		<!-- Link -->
		<?php if(!empty(get_field('link'))):
			$link = get_field('link');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button-dot button-big" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; 
		endif; ?>

		<?php 
		if($image) {
		echo '</div>'; //End of div.text-container;
		} ?>

<?php endif; ?>

</section>


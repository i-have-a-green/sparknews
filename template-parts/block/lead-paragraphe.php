<?php
/**
 * Block Name: lead-paragraphe
 */
 ?>
  
<div class="blk-lead padding-b">

    <?php
    $title = get_field('lead_paragraphe');
    $content = get_field('lead_paragraphe_small');
    if ( !$title ) :?>
        <em>Renseigner le bloc</em>
    <?php 
    else :

        echo '<div class="big-lead entry-content no-useless-margin">'. $title .'</div>';
        if ($content) {
            echo '<div class="small-lead entry-content no-useless-margin">'. $content .'</div>';
        }
        
    endif; 
    ?>

</div>

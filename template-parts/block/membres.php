<?php
/**
 * Block Name: Intervenants
 */
 ?>

<?php
$title = get_field('title');
$text = get_field('text');
?>

<section class="blk-member wrapper no-useless-margin white brand-blue-bg <?php if( $title ) { echo 'has-title';} ?>">

<?php
if ( empty($title) ):?>
    <em>Renseigner le bloc</em>
<?php else :?>

    <?php // Title
    if($title) {

        // Title with icon (Stratus)
        $icon = get_field('activate_icon');

        if ( $icon ) {

            $style = get_field('icon_style');
            if (!$style) {
                $style = 'style_1';
                // If no style selected, use style_1 by default
            }

            echo '<h2 class="h1-like title_with_icon '. $style .'">'. $title .'</h2>';

        } else {
            echo '<h2 class="h1-like">'. $title .'</h2>';
        }

    }?>


    <div class="text-container">
        <?php if(!empty($text)):?>
            <div class="entry-content"><?php echo $text; ?></div>
        <?php endif; ?>

        <?php
        // Link CTA (optionel)
        $cta = get_field('cta');
        if( $cta ): 
            $cta_url = $cta['url'];
            $cta_title = $cta['title'];
            $cta_target = $cta['target'] ? $cta['target'] : '_self';
            ?>
            <a class="button-dot button-color-bg" href="<?php echo esc_url( $cta_url ); ?>" target="<?php echo esc_attr( $cta_target ); ?>"><?php echo esc_html( $cta_title ); ?></a>
        <?php endif; 
        ?>

        <?php
        // Link Archive (-> list of all the members)
        $show_archive_link = get_field('show_archive_link');
        $archive_page = get_field('list_member', 'option');
        
        if ($show_archive_link && $archive_page ) {
            echo '<a class="button-dot button-color-bg" href="'. esc_url( $archive_page ) .'#memberListing">';

                if ( get_field('archive_text') ) {
                    the_field('archive_text');
                } else {
                    esc_html_e('Découvrir tous les intervenant•es', 'sparknews');
                }

            echo '</a>';
        }
        ?>
    </div>
        
    <div class="basic-listing-member">
    <?php // Check rows exists.
        if( have_rows('members') ):

            // Loop through rows.
            while( have_rows('members') ) : the_row();

                // Load sub field value.
                $objet_partner = get_sub_field('member');
                ?>
            
                <div class="member-single">
                
                    <div class="member-thumbnail brand-black-bg image-container">
                        <?php 
                        if ( has_post_thumbnail($objet_partner) ) {
                            echo get_the_post_thumbnail($objet_partner, 'member');
                        } else {
                            $image = get_field('imageFallback', 'option');
                            echo wp_get_attachment_image( $image, 'member' );
                        }
                        ?>
                    </div>

                    <h3 class="member-name"><?php echo get_the_title($objet_partner); ?></h3>
                    <p><?php the_field( "description", $objet_partner->ID ); ?></p>
                </div>

            <?php
            // End loop.
            endwhile;

        endif;
    ?>

    </div><!-- /basic-listing -->       
	
<?php endif; ?>

</section>


<?php
/**
 * Block Name: bloc accueil
 */
 ?>

<section class="blk-tools v-padding-regular no-useless-margin">

<?php

if ( empty(get_field('title')) ):?>

	<em>Renseigner le bloc</em>
		
<?php else :?>

	<div class="responsive-text-container no-useless-margin custom-wrapper">

		<div>

			<?php 
			// Title with icon (Stratus)
			$title = get_field('title');
			if($title) {

				$icon = get_field('activate_icon');
				if ( $icon ) {

					$style = get_field('icon_style');
					if (!$style) {
						$style = 'style_1';
						// If no style selected, use style_1 by default
					}

					echo '<h2 class="h1-like brand-green title_with_icon '. $style .'">'. $title .'</h2>';

				} else {
					echo '<h2 class="h1-like brand-green">'. $title .'</h2>';
				}
				
			} ?>

			<?php 
			if(!empty(get_field('link'))):
				$link = get_field('link');
				if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button-dot" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; 
			endif; 
			?>

		</div>

		<div>
			<?php if(!empty(get_field('text'))):?>
				<div class="entry-content"><?php echo get_field('text'); ?></div>
			<?php endif; ?>
		</div>
		
	</div>

	<!-- Listing Tools Archive -->
	<div class="listing-archive">
		<?php

		$arg = array(
			'posts_per_page' => 3,
			'orderby'        => 'rand',
			'include'          => get_field('boite_a_outils'),
			'post_status' 	 => 'publish',
			'post_type'		 => 'toolbox',
			);
			// var_dump($arg);

		$posts = get_posts( $arg );

		// var_dump($posts );
		global $post;

		foreach($posts as $post): 
			
			setup_postdata( $post );
			
			get_template_part( 'template-parts/standard', get_post_type() );

		endforeach; 

		wp_reset_postdata();
		
		?>
	</div><!-- End of Lisitng -->

<?php endif; ?>

</section>


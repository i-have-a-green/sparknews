<?php
/**
 * Template part for displaying page archive-toolbox in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="article article-tools">

	<!-- Thumbnail -->
	<a class="thumbnail image-container" href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php 
			$size = 'free-height';
			if ( has_post_thumbnail() ) { ?>
				<img src="<?php echo get_the_post_thumbnail_url($post, $size);?>" alt="<?php echo get_the_post_thumbnail_caption();?>">
			<?php
			} else {
				$size = 'thumbnail-standard';
				$image = get_field('imageFallback', 'option');
				echo wp_get_attachment_image( $image, $size );
			} 
		?>
	</a>

	<!-- Title -->
	<a class="h3-like article-title link-discrete" href="<?php the_permalink();?>">
		<?php the_title();?>
	</a>

	<!-- Category -->
	<?php
	$term = get_the_terms($post, 'taxo_toolbox');
	if($term):

		echo '<div class="list list-category white small-text">';

		foreach ($term as $row):

			echo '<a class="button" href="'. get_category_link($row) .'">';
			lettrine($row);
			echo $row->name;
			echo '</a>';
			
		endforeach;

		echo '</div>';
		
	endif; 
	?>

	<?php 
	/* Keywords

	$all_keyWords = get_the_terms($post, "taxo_tag");
	if($all_keyWords):

	echo '<div class="list list-tag small-text">';

		foreach ($all_keyWords as $row):
				echo '<a class="button" href="'. get_category_link($row).'">#'.$row->name.'</a>';
		endforeach;

	echo '</div>';
		 
	endif;
	*/
	?>

</article>



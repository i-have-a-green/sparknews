<?php
/**
 * Template part for displaying page archive-training in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>

<article class="article article-learn">

	<!-- Thumbnail -->
	<a class="thumbnail image-container"  href="<?php the_permalink();?>" title="<?php the_title();?>">
		<?php 
			$size = 'free-height';
			if ( has_post_thumbnail() ) { ?>
				<?php the_post_thumbnail( 'free-height'); ?>
			<?php
			} else {
				$image = get_field('imageFallback', 'option');
				echo wp_get_attachment_image( $image, 'thumbnail-standard' );
			} 
		?>
	</a>

	<!-- Type -->
	<?php
	$ref = ihag_get_term($post, 'taxo_reference');
	echo '<i class="body-like small-text article-type title-font">'. $ref->name . '</i>';
	?>

	<!-- Title -->
	<a class="h3-like article-title link-discrete" href="<?php the_permalink();?>">
		<?php the_title();?>
	</a>

	<!-- Category -->
	<?php
	$term = get_the_terms(get_the_id(), 'taxo_training');
	if($term):

		echo '<div class="list list-category small-text white">';

		foreach ($term as $row):

			echo '<a class="button" href="'. get_category_link($row) .'">';
			lettrine($row);
			echo $row->name;
			echo '</a>';

		endforeach;

		echo '</div>';
		
	endif; 
	?>
	
	<!-- Keywords -->
	<?php 
		$all_keyWords = get_the_terms(get_the_id(), "taxo_tag");
		if($all_keyWords):

		echo '<div class="list list-tag small-text">';

			foreach ($all_keyWords as $row):
				echo '<a class="button" href="'. get_permalink(get_field('archive_training', 'option')).'?var_taxo_tag'.$row->term_id.'">#'.$row->name.'</a>';
			endforeach;

		echo '</div>';
		 
		endif; 
	?>

</article>



if (document.forms.namedItem("contactForm")) {
    document.forms.namedItem("contactForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("contactForm");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'contactForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('contactForm').classList.add("hideForm");
                document.getElementById('sendMessage').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
                
            }
        };
        xhr.send(formData);
    });
}



// if (document.forms.namedItem("footer-newslettter-form")) {
//     document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
//         document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
//         e.preventDefault();

//         var form = document.forms.namedItem("footer-newslettter-form");
//         var formData = new FormData(form);
//         xhr = new XMLHttpRequest();
//         xhr.open('POST', resturl + 'formNewsletter', true);
//         xhr.onload = function() {
//             if (xhr.status === 200) {
//                 document.getElementById('sendMessageNewsletter').disabled = false;
//                 document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
//                 document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
//                 document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
//             }
//         };
//         xhr.send(formData);
//     });
// }

var prefix = "slide";
var slidecount = 0;
var bgcount = 0;
var blkHeaderHome = document.getElementById("blk-header-home");

if ( blkHeaderHome ) {

    // Animation on click
    blkHeaderHome.addEventListener('click', function(e) {

        e.preventDefault();
        animateLogo();

    });

    // Animation on hover
    var logo = document.getElementById("logo-home");
    logo.addEventListener('mouseenter', function(e){

        animateLogo();

    });
}

function animateLogo() {

        // On anime la couleur d'arrière-plan
        if (bgcount <= 5) {
            
            if (bgcount < 5) {
                if (bgcount == 0) {
                    // Step 2 : blue background 
                    blkHeaderHome.classList.remove("brand-violet-bg");
                    blkHeaderHome.classList.add("brand-blue-bg");
                }
                if (bgcount == 1) {
                    // Step 3 : green background 
                    blkHeaderHome.classList.remove("brand-blue-bg");
                    blkHeaderHome.classList.add("brand-green-bg");
                }
                if (bgcount == 2) {
                    // Step 4 : yellow background 
                    blkHeaderHome.classList.remove("brand-green-bg");
                    blkHeaderHome.classList.add("brand-violet-bg");
                }
                if (bgcount == 3) {
                    // Step 5 : orange background 
                    blkHeaderHome.classList.remove("brand-violet-bg");
                    blkHeaderHome.classList.add("brand-orange-bg");
                }
                if (bgcount == 4) {
                    // Step 6 : pink background 
                    blkHeaderHome.classList.remove("brand-orange-bg");
                    blkHeaderHome.classList.add("brand-pink-bg");
                }
                bgcount++;

            } else {
                // Step 1 & 7 : violet background
                bgcount = 0;
                blkHeaderHome.classList.remove("brand-pink-bg");
                blkHeaderHome.classList.add("brand-violet-bg");
            }
        }


        // On anime le logo
        if (slidecount <= 9) {
            
            var currentslide = prefix.concat(slidecount);
            var nextslide = prefix.concat(slidecount+1);
            var firstslide = prefix.concat('0');
            
            if (slidecount < 9) {
                // Pour les Slides de 1 à 8 : on fait apparaitre la slide suivante
                document.getElementById(currentslide).classList.remove("active");
                document.getElementById(nextslide).classList.add("active");
                slidecount++;
            } else {
                // Pour la slide 8 : on fait réapparaitre la slide 1
                document.getElementById(currentslide).classList.remove("active");
                document.getElementById(firstslide).classList.add("active");
                slidecount = 0;
            }

        } else {
            slidecount = 0;
            console.log ("Erreur dans l'animation du logo");
        }

        logo.classList.add("active");
        window.setTimeout( function() {
            logo.classList.remove("active");
        }, 500); //fréquence du scroll
}
/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            var modaleContent = document.createElement("div");
            modaleContent.id = 'modaleContent' + el.dataset.uniqId;
            modaleContent.classList.add("modaleContent");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

            for (var key in iframe) {
                // check if the property/key is defined in the object itself, not in parent
                if (iframe.hasOwnProperty(key)) {
                    if (key == el.dataset.uniqId) {
                        document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = iframe[key];
                        modale.classList.add("active");
                    }
                }
            }
        });
    });
});
document.addEventListener('DOMContentLoaded', function() {

    var randomColor = document.getElementById("has-random-color");

    if (randomColor) {

        // Choisis un chiffre au hasard entre 1 et 6 
        var rand = Math.floor(Math.random() * 6) + 1 ;
        var color = 'random-blue';

        if (rand == 1) {
            color = 'random-blue';
        } else if (rand == 2) {
            color = 'random-orange';
        } else if (rand == 3) {
            color = 'random-pink';
        } else if (rand == 4) {
            color = 'random-green';
        } else if (rand == 5) {
            color = 'random-yellow';
        } else if (rand == 6) {
            color = 'random-violet';
        } else {
            console.log('Sorry, random number selection has failled');
        }

        /*
        // Debug
        console.log('Nombre alétoire :');
        console.log(rand);
        console.log('Couleur :');
        console.log(color);
        */

        randomColor.classList.add(color);
    }    
});
// organise - infinit scroll 
if (document.querySelector('#infinite-list')) {
    var base_url = window.location.href;
    var elm = document.querySelector('#infinite-list');
    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);
    formData.append("taxo_tag_var", elm.dataset.taxo_tag);

    console.log("readMorePost");

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            console.log(xhr.response);
            elm.insertAdjacentHTML('beforeend', xhr.response);
            // window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            var width = 320,
                height = 400,
                left = (widthScreen - width) / 2,
                top = (heightScreen - height) / 2,
                url = this.href,
                opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });

    copyLink = document.getElementsByClassName('copyLink');
    Array.prototype.forEach.call(copyLink, function(el) {
        el.addEventListener("click", function(e) {
            e.stopPropagation();
            copyLinkValue = el.getElementsByClassName('copyLinkValue');
            copyLinkValue[0].focus();
            copyLinkValue[0].select();
            document.execCommand("copy");
            el.classList.add('active');
            setTimeout(function(){ 
                el.classList.remove('active');
            }, 3000);
            
        });

    });
});
/*

Sticky content for single.php, single-training.php & single-tools.php

*/

window.addEventListener('scroll', function(e) {

    var container = document.getElementById("scroll-content");

    if (container) {

        var menu = document.getElementById("sticky-content");
        var stickyStart = container.offsetTop;
        var stickyStop = container.offsetHeight;
        var menuHeight = menu.offsetHeight;
        var derniere_position_de_scroll_connue = 0;
        var ticking = false;

        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
    
                if (window.pageYOffset > stickyStart) {
                    // Scroll start
                    menu.classList.add("sticky");
    
                    if (window.pageYOffset > ( stickyStart + stickyStop - menuHeight )) {
                        // Scroll after container
                        menu.classList.add("sticky-finish");
                    } else {
                        // Scroll after container
                        menu.classList.remove("sticky-finish");
                    }
                } else {
                    // Scroll didn't start
                    menu.classList.remove("sticky");
                }
            }, 50); //fréquence du scroll
        }

        ticking = true;
    }
    
});


// 4 - Modif topbar background on scroll

/*window.onscroll = function() {

    var menu = document.getElementById("scroll-anchor");
    var header = document.getElementById("topbar-wrapper");
    var sticky = menu.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("scroll");
        window.onscroll = function() { return; };
    } else {
        header.classList.remove("scroll");
    }
};*/

// var derniere_position_de_scroll_connue = 0;
// var ticking = false;
// var menu = document.getElementById("scroll-anchor");
// var header = document.getElementById("topbar-wrapper");
// var sticky = menu.offsetTop;

// window.addEventListener('scroll', function(e) {
//     if (!ticking) {
//         window.setTimeout(function() {
//             ticking = false;
//             if (window.scrollY > sticky) {
//                 header.classList.add("scroll");
//             } else {
//                 header.classList.remove("scroll");
//             }
//         }, 300); //fréquence du scroll
//     }
//     ticking = true;
// });
document.addEventListener('DOMContentLoaded', function() {
    var title = document.getElementsByClassName("title_with_icon");
    var letters = 5;

    Array.prototype.forEach.call(title, function(string) {
        var str = string.innerText; 


        var cleanContent = str.replace( /[\r\n]+/gm, " " ); 

        var newContent = cleanContent.split(" ").map( function (x) {
        if (x.length > letters) {
            return "<span>" + x + "</span>";
        } else {
            return x;
        }
        });
    
        string.innerHTML = newContent.join(" ");
    });

});
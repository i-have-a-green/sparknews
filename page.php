<?php get_header(); ?>

<!-- Begining of the loop -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php

	echo '<div id="has-random-color" class="img-white-and-black">';

	echo '<header class="wrapper custom-color-bg">';
		wpBreadcrumb();
		echo '<h1 class="padding-b">'; 
		lettrine($post); 
		echo get_the_title() .'</h1>';
	echo '</header>';
		
?>
<main id="raw-content">
	<?php the_content(); ?>
</main>

<?php if ( !is_front_page() ){

echo '</div><!--/#has-random-color -->';	
} ?>


<!-- End of the loop -->
<?php endwhile; endif;?>

<?php
get_footer();




	</div><!-- div #content -->

	<footer id="footer" class="wrapper title-font small-text <?php if (is_front_page()) { echo 'brand-black-bg';} ?>">

		<!-- Footer Part 1 : Social Media -->
		<nav id="footer-social">

			<?php 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
			$link = get_field('facebook', 'option');

			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; ?>

			<?php 
			$link = get_field('instagram', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
				<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; ?>

			<?php 
			$link = get_field('twitter', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
				<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; 
			?>

			<?php 
			$link = get_field('linkedin', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
				<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; ?>

			<?php 
			$link = get_field('medium', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
				<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; ?>

			<?php 
			$link = get_field('youtube', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
				<p>&rarr; <?php echo $link_title ?></p>
				</a>
			<?php endif; ?>

			<!--  Footer Part 2 : Copyright -->
			<?php 
			$copyright_footer = get_field('copyright', 'option');
			if ($copyright_footer) { 
				?>
				<div id="footer-copyright"><?php echo $copyright_footer; ?></div>
				<?php
			}?>

		</nav>

		<!--  Footer Part 3 : Divers liens en footer -->			
		<?php 
		$text_footer = get_field('content', 'option');
		if ($text_footer) { 
			?>
			<div id="footer-content">		
				<?php echo $text_footer; ?>
			</div>
			<?php
		}?>

	</footer><!-- End of #footer -->

<?php wp_footer(); ?>

</body>
</html>
